package pl.edu.pwsztar.service;

@FunctionalInterface
public interface Converter <F,T>{
    T convert(F from);
}
