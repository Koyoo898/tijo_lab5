package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.service.Converter;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MovieListMapper implements Converter<List<Movie>,List<MovieDto>>{

    private MovieDto cMovie(Movie from) {
        MovieDto movieDto = new MovieDto();
        movieDto.setMovieId(from.getMovieId());
        movieDto.setTitle(from.getTitle());
        movieDto.setImage(from.getImage());
        movieDto.setYear(from.getYear());
        return movieDto;
    }

    @Override
    public List<MovieDto> convert(List<Movie> from) {
        List<MovieDto> movieDtos = from
                .stream()
                .map(this::cMovie)
                .collect(Collectors.toList());
        return movieDtos;
    }
}
